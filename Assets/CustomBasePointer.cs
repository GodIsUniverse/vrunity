﻿// Copyright 2017 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using UnityEngine.EventSystems;

/// This abstract class should be implemented for pointer based input, and used with
/// the GvrPointerInputModule script.
///
/// It provides methods called on pointer interaction with in-game objects and UI,
/// trigger events, and 'BaseInputModule' class state changes.
///
/// To have the methods called, an instance of this (implemented) class must be
/// registered with the **GvrPointerManager** script in 'Start' by calling
/// GvrPointerInputModule.OnPointerCreated.
///
/// This abstract class should be implemented by pointers doing 1 of 2 things:
/// 1. Responding to movement of the users head (Cardboard gaze-based-pointer).
/// 2. Responding to the movement of the daydream controller (Daydream 3D pointer).
public abstract class CustomBasePointer : GvrBasePointer {


    /// If true, the trigger was just pressed. This is an event flag:
    /// it will be true for only one frame after the event happens.
    /// Defaults to GvrControllerInput.ClickButtonDown, can be overridden to change the trigger.
    // public override bool TriggerDown {
    //   get {
    //     bool isTriggerDown = Input.GetMouseButtonDown(0);
    //     return isTriggerDown || GvrControllerInput.ClickButtonDown;
    //   }
    // }

    /// joystick button 0 = A
    /// joystick button 1 = B
    /// joystick button 2 = X
    /// joystick button 3 = Y
    /// joystick button 4 = LT
    /// joystick button 5 = RT
    public override bool TriggerDown {
    get {
        bool isTriggerDown = Input.GetButtonDown("A");
        return isTriggerDown;
    }
  }

  /// If true, the trigger is currently being pressed. This is not
  /// an event: it represents the trigger's state (it remains true while the trigger is being
  /// pressed).
  /// Defaults to GvrControllerInput.ClickButton, can be overridden to change the trigger.
  // public override bool Triggering {
  //   get {
  //     bool isTriggering = Input.GetMouseButton(0);
  //     return isTriggering || GvrControllerInput.ClickButton;
  //   }
  // }
  public override bool Triggering {
    get {
        bool isTriggering = Input.GetKey("x");
        return isTriggering;
    }
  }
  /// If true, the trigger was just released. This is an event flag:
  /// it will be true for only one frame after the event happens.
  /// Defaults to GvrControllerInput.ClickButtonUp, can be overridden to change the trigger.
  // public override bool TriggerUp {
  //   get {
  //     bool isTriggerDown = Input.GetMouseButtonUp(0);
  //     return isTriggerDown || GvrControllerInput.ClickButtonUp;
  //   }
  // }  
  public override bool TriggerUp {
    get {
        bool isTriggerDown = Input.GetKeyDown("x");
        return isTriggerDown;
    }
  }
}
