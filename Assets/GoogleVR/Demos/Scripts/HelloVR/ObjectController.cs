// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace GoogleVR.HelloVR {
  using UnityEngine;
  using System.Collections;
    using System.Linq;
    using System.Collections.Generic;

    [RequireComponent(typeof(Collider))]
  public class ObjectController : Photon.PunBehaviour {

    private Vector3 startingPosition;
    private Renderer myRenderer;
    public List<int> pointsDictionaryIds;
    public List<int> pointsDictionaryPoints;
    public Material inactiveMaterial;
    public Material gazedAtMaterial;

    void Start() {
      startingPosition = transform.localPosition;
      myRenderer = GetComponent<Renderer>();      
      OnSetGazedAt(false);
    }

    public void OnClickDown()
    {      
        int sibIdx = transform.GetSiblingIndex();
        int numSibs = transform.parent.childCount;
        sibIdx = (sibIdx + Random.Range(1, numSibs)) % numSibs;
        int randomRangeYAngle = Random.Range(-90, 90);
        float randomDistance = 2 * Random.value + 1.5f;
        // sibIdx = (sibIdx + Random.Range(1, numSibs)) % numSibs;
        photonView.RPC("AddPointToPlayer", PhotonTargets.All);
        photonView.RPC("TeleportRandomly", PhotonTargets.All, sibIdx, randomRangeYAngle, randomDistance);
    }

    public void OnSetGazedAt(bool gazedAt)
    {
        photonView.RPC("SetGazedAt", PhotonTargets.All, gazedAt);
    }

    [PunRPC]
    public IEnumerator SetGazedAt(bool gazedAt) {
      if (inactiveMaterial != null && gazedAtMaterial != null) {
        myRenderer.material = gazedAt ? gazedAtMaterial : inactiveMaterial;
        yield return null;
      }
    }

    public void Reset() {
      int sibIdx = transform.GetSiblingIndex();
      int numSibs = transform.parent.childCount;
      for (int i=0; i<numSibs; i++) {
        GameObject sib = transform.parent.GetChild(i).gameObject;
        sib.transform.localPosition = startingPosition;
        sib.SetActive(i == sibIdx);
      }
    }

    public void Recenter() {
#if !UNITY_EDITOR
      GvrCardboardHelpers.Recenter();
#else
      if (GvrEditorEmulator.Instance != null) {
        GvrEditorEmulator.Instance.Recenter();
      }
#endif  // !UNITY_EDITOR
    }

    [PunRPC]
    public IEnumerator TeleportRandomly(int sibIdx, int randomRangeYAngle, float randomDistance) {
      // Pick a random sibling, move them somewhere random, activate them,
      // deactivate ourself.
      // int sibIdx = transform.GetSiblingIndex();
      // // int numSibs = transform.parent.childCount;
      // sibIdx = (sibIdx + randomRangeSibling) % numSibs;
      // sibIdx = (sibIdx + Random.Range(1, numSibs)) % numSibs;
      GameObject randomSib = transform.parent.GetChild(sibIdx).gameObject;

      // Move to random new location ±100º horzontal.
      Vector3 direction = Quaternion.Euler(
          0,
          randomRangeYAngle,
          0) * Vector3.forward;
      // New location between 1.5m and 3.5m.
      
      Vector3 newPos = direction * randomDistance;
      // Limit vertical position to be fully in the room.
      newPos.y = Mathf.Clamp(newPos.y, -1.2f, 4f);
      randomSib.transform.localPosition = newPos;

      randomSib.SetActive(true);
      OnSetGazedAt(false);
      gameObject.SetActive(false);
      
      yield return null;
    }

    [PunRPC]     
    public IEnumerator AddPointToPlayer(){      
        if(pointsDictionaryIds.Contains(photonView.ownerId)) {
          pointsDictionaryPoints[pointsDictionaryIds.IndexOf(photonView.ownerId)]++;           
        } else {
          pointsDictionaryIds.Add(photonView.ownerId);
          pointsDictionaryPoints.Add(1);
        }        
        Debug.Log(photonView.ownerId);
        Debug.Log(pointsDictionaryPoints[photonView.ownerId]);   
        return null;  
    }
  }
}
