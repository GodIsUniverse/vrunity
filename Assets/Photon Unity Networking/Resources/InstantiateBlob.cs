﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InstantiateBlob : MonoBehaviour 
{
    Color[] ColorS = new Color[]{
        Color.black, 
        Color.blue, 
        Color.cyan, 
        Color.gray, 
        Color.green, 
        Color.grey, 
        Color.magenta, 
        Color.red, 
        Color.white, 
        Color.yellow
    };
    
    void OnJoinedRoom()
    {
        CreatePlayerObject();
    }

    void CreatePlayerObject()
    {
        Vector3 position = new Vector3( 3f, 1.5f, 3f );
        GameObject newPlayerObject = PhotonNetwork.Instantiate( "PlayerBlob", position, Quaternion.identity, 0 );
        newPlayerObject.GetComponent<MeshRenderer>().material.color = ColorS[Random.Range(0,9)];

        

        // Camera.Target = newPlayerObject.transform;
    }
}
