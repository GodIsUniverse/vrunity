﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControllerInput : Photon.PunBehaviour  {

    List<Camera> globCameras;    
    PhotonView m_PhotonView;

	// Use this for initialization
	void Start () {
        
        m_PhotonView = GetComponent<PhotonView>();

        
    }

    

	// Update is called once per frame    
	public float horizontalSpeed = 3F;
    public float verticalSpeed = 3F;
	void Update () {        

			//this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + 0.1f, this.gameObject.transform.position.y + 0.1f, 0);            
        if(m_PhotonView.isMine == false && PhotonNetwork.connected == true) {
            
            return;
        }

        float h = horizontalSpeed * Input.GetAxis("Horizontal");
        float v = verticalSpeed * Input.GetAxis("Vertical");
        v += Input.GetButton("A") ? 3 * verticalSpeed : 0;
        // Input.GetButtonDown("Fire1")        

        if (Input.GetAxis("LSB") != 0 || Input.GetAxis("LT") != 0)
        {
            // Debug.Log("here");
            // Debug.Log(Input.GetAxis("LBTA"));
            h *= 2;
            v *= 2;
        }
        
        // var test = this.gameObject.GetComponentInChildren<Camera>();       
        
        // Debug.Log("photonView.ownerId: " + photonView.ownerId);
        // Debug.Log("test: " + test.GetInstanceID());
        
        
        // Camera.allCameras.ToList().ForEach(x => {Debug.Log("test: " + test.GetInstanceID()); Debug.Log("x: " + x.GetInstanceID()); x.enabled = x.GetInstanceID() == test.GetInstanceID();});   
        
        

        transform.Translate(
            h * Time.deltaTime * Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y)
            + v * Time.deltaTime * Mathf.Sin(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y),
            0,
            v * Time.deltaTime * Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y) 
            - h * Time.deltaTime * Mathf.Sin(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y)
        );

        var head = transform.GetChild(0);
        
        var constrainUp = Camera.main.transform.eulerAngles.x < 290 && Camera.main.transform.eulerAngles.x > 250;
        var constrainDown = Camera.main.transform.eulerAngles.x < 150 && Camera.main.transform.eulerAngles.x > 30;

        head.rotation = Quaternion.Euler((!constrainUp && !constrainDown) ? Camera.main.transform.eulerAngles.x : head.rotation.eulerAngles.x
        , Camera.main.transform.eulerAngles.y, Camera.main.transform.eulerAngles.z);


        // if(Mathf.Abs(Camera.main.transform.eulerAngles.x) < 70)
        // {
        //     Debug.Log("here");
        //     head.transform.position = Quaternion.Euler(Camera.main.transform.eulerAngles);
        // }

        //head.transform.LookAt(this.gameObject.transform);

        // transform.Rotate(            h * Time.deltaTime * Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y)
        //     + v * Time.deltaTime * Mathf.Sin(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y),
        //     0,
        //     v * Time.deltaTime * Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y) 
        //     - h * Time.deltaTime * Mathf.Sin(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y));

        //var validCameras = ValidCameras();
        //    while (validCameras.MoveNext()) {
        //    Camera cam = validCameras.Current;

        //    cam.transform.position = new Vector3(cam.transform.position.x + 0.1f, cam.transform.position.y + 0.1f, 0);
        // Debug.Log(cam.transform.position);
        //    //cam.transform.localRotation = HeadRotation;
        //  }
    }

    //     public override void OnPhotonPlayerConnected	(	PhotonPlayer 	newPlayer	)	
    // {        
    //     photonView.RPC("UpdateCameras", PhotonTargets.AllBuffered, newPlayer);        
    // }


    // [PunRPC]
    // public IEnumerator UpdateCameras(PhotonPlayer newPlayer) {
    //     if(m_PhotonView.isMine == true && PhotonNetwork.connected == true) {            
    //         yield return null;;
    //     } else {

    //     Debug.Log(this.photonView.owner.UserId);
    //     Debug.Log(PhotonNetwork.player.UserId);
    //     Debug.Log(newPlayer.UserId);
        
    //     var test = this.gameObject.GetComponentInChildren<Camera>().GetInstanceID();       
    //     Debug.Log(test);
    //     Debug.Log("photonView.ownerId2: " + photonView.ownerId);
    //     Camera.allCameras.ToList().ForEach(x => {Debug.Log("test2: " + test); Debug.Log("x2: " + x.GetInstanceID()); x.enabled = x.GetInstanceID() == test;});   
    //     }
    //     // if(PhotonNetwork.player != newPlayer) {
    //     //     yield return null;
    //     // };
    //     // // globCameras = GetCameras();
    //     // // globCameras.ToList().ForEach(x=> Debug.Log(x));
    //     // // globCameras.ToList().Count();
        
    //     // yield return new WaitForSeconds(3f);
    //     // Debug.Log("new player");
    //     // Debug.Log(newPlayer);
    //     // var test = photonView.gameObject.GetComponentInChildren<Camera>();       
    //     // Debug.Log("test in PunPRC: " + test.GetInstanceID());
    //     // // Debug.Log("CAMERA LENGTHS: " + Camera.allCameras.Length);
    //     // // Debug.Log("CAMERA ALL CAMERAS: " + Camera.allCameras);
    //     // // // Input.GetJoystickNames().ToList().ForEach(x => Debug.Log(x));
    //     yield return null;
    // }

  private IEnumerator<Camera> ValidCameras() {
    for (int i = 0; i < Camera.allCameras.Length; i++) {
      Camera cam = Camera.allCameras[i];
      if (!cam.enabled || cam.stereoTargetEye == StereoTargetEyeMask.None) {
        continue;
      }

      yield return cam;
    }
  }

    private List<Camera> GetCameras()
    {
        List<Camera> cameras = new List<Camera>();
        for (int i = 0; i < Camera.allCameras.Length; i++)
        {
            Camera cam = Camera.allCameras[i];
            if (!cam.enabled || cam.stereoTargetEye == StereoTargetEyeMask.None)
            {
                continue;
            }

            cameras.Add(cam);
        }

        return cameras;
    }
}
